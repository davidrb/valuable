#include <valuable/valuable.hpp>

#include <catch2/catch.hpp>
#include <sstream>

namespace valuable_tests {
  // example value_object
  class person {
    FIELDS(
      person,
      name, age, phone_number,
      credit_card
    );

    TYPE( age, int );
    TYPE( name, std::string );
    TYPE( phone_number, std::string );

    TYPE( credit_card, std::optional<std::string> );
  };
}

using valuable_tests::person;

TEST_CASE("create new object with '*<class>::with()'") {
  auto david = *person::with()
    .name("David")
    .age(23)
    .phone_number("(808) 757-1234");

  REQUIRE(name(david) == "David");
  REQUIRE(age(david) == 23);
  REQUIRE(phone_number(david) == "(808) 757-1234");
}

/**
 * with() requires all fields not wrapped in std::optional
 * to be set, or it will static_assert.
 *
 * Setting a field more than once in a with() or update()
 * will cause a static_assert.
 */

TEST_CASE("create object based on existing object with *<object>.update()") {
  auto david = *person::with()
    .name("David")
    .age(23)
    .phone_number("(808) 757-1234");

  auto bob = *david.update()
    .name("Bob");

  REQUIRE(name(bob) == "Bob");
  REQUIRE(age(bob) == age(david));
  REQUIRE(phone_number(bob) == phone_number(david));
}

TEST_CASE("update an object with lambdas") {
  auto david = *person::with()
    .name("David")
    .age(23)
    .phone_number("(808) 757-1234");

  auto david_jr = *david.update()
    .name([](auto old){ return old + " Jr."; });

  REQUIRE(name(david_jr) == "David Jr.");
}

TEST_CASE("objects are streamable") {
  auto p = *person::with()
    .name("David")
    .age(23)
    .phone_number("(808) 757-1234");

  auto ss = std::stringstream{};
  ss << p;
  auto result = std::string{std::istreambuf_iterator<char>{ss}, {}};

  REQUIRE( result == "person{ :name \"David\" :age 23 :phone_number \"(808) 757-1234\" :credit_card <nil> }" );

  p = p.update()
    .credit_card("12345")
    .name("Bob");
}
