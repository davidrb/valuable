#ifndef VALUABLE_HPP
#define VALUABLE_HPP

#include <boost/preprocessor.hpp>
#include <tuple>
#include <map>
#include <iostream>
#include <optional>
#include <functional>

namespace valuable {
  template<typename T>
  struct is_optional {
    static constexpr auto value = false;
  };

  template<typename T>
  struct is_optional<std::optional<T>> {
    static constexpr auto value = true;
  };

  template<typename T>
  struct remove_optional {
    using type = T;
  };

  template<typename T>
  struct remove_optional<std::optional<T>> {
    using type = T;
  };

  template<typename T>
  struct printer {
    static void print(std::ostream& os, T const& t) {
      os << t;
    }
  };

  template<typename T>
  struct printer<std::optional<T>> {
    static void print(std::ostream& os, std::optional<T> const& s) {
      if (s) printer<T>::print(os, *s);
      else os << "<nil>";
    }
  };

  template<typename K, typename V>
  struct printer<std::map<K, V>> {
    static void print(std::ostream& os, std::map<K, V> const& t) {
      os << "std::map{";
      for (auto kv : t) {
        os << " ";
        valuable::printer<K>::print(os, kv.first);
        os << " ";
        valuable::printer<V>::print(os, kv.second);
      }
      os << " }";
    }
  };

  template<>
  struct printer<std::string> {
    static void print(std::ostream& os, std::string const& s) {
      os << "\"" << s << "\"";
    }
  };
}

#define VALUABLE_DELEGATE_COMPARISON_OPERATOR(op) \
  template<typename T> inline auto operator op (T const& rhs) const -> typename std::enable_if<std::is_same<T, valuable_type_>::value, bool>::type { return to_tuple() op rhs.to_tuple(); } \

#define VALUABLE_COMPARISON_OPERATORS \
  VALUABLE_DELEGATE_COMPARISON_OPERATOR(==); \
  VALUABLE_DELEGATE_COMPARISON_OPERATOR(!=); \
  VALUABLE_DELEGATE_COMPARISON_OPERATOR(<); \
  VALUABLE_DELEGATE_COMPARISON_OPERATOR(>); \
  VALUABLE_DELEGATE_COMPARISON_OPERATOR(<=); \
  VALUABLE_DELEGATE_COMPARISON_OPERATOR(>=); \

#define OUTPUT_FIELD(r, d, f) \
  os << " :" << BOOST_PP_STRINGIZE(f) << " ";   \
  ::valuable::printer<decltype(x.BOOST_PP_CAT(f, _))>::print(os, x.BOOST_PP_CAT(f, _)); \

#define VALUABLE_OSTREAM_OPERATORS(...) \
  template<typename T> \
  friend inline auto operator<<(std::ostream& os, T const& x) -> typename std::enable_if<std::is_same<T, valuable_type_>::value, std::ostream&>::type { \
    os << valuable_name_ << "{"; \
    BOOST_PP_SEQ_FOR_EACH(OUTPUT_FIELD, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)); \
    os << " }"; \
    return os; \
  } \

#define VALUABLE_META(name) \
  using valuable_type_ = name; \
  static constexpr auto valuable_name_ = BOOST_PP_STRINGIZE(name); \

#define VALUABLE_WITH_PROXY_FIELD(r, d, field) \
  auto field (typename ::valuable::remove_optional<decltype(T::BOOST_PP_CAT(field, _))>::type v) -> BOOST_PP_CAT(after_, field)& { \
    static_assert( !BOOST_PP_CAT(field, _initialized), "field already initialized" ); \
    instance_.BOOST_PP_CAT(field, _) = v; \
    return *(BOOST_PP_CAT(after_, field) *)this; \
  } \
  template<typename Arg> \
  auto field(Arg f) -> typename std::enable_if<std::is_invocable<Arg, decltype(T::BOOST_PP_CAT(field, _))>::value, BOOST_PP_CAT(after_, field)&>::type { \
    static_assert( !BOOST_PP_CAT(field, _initialized), "field already initialized" ); \
    instance_.BOOST_PP_CAT(field, _) = f(std::move(instance_.BOOST_PP_CAT(field, _))); \
    return *(BOOST_PP_CAT(after_, field) *)this; \
  } \
  template<typename Arg> \
  auto field(Arg arg) -> typename std::enable_if<!std::is_invocable<Arg, decltype(T::BOOST_PP_CAT(field, _))>::value, BOOST_PP_CAT(after_, field)&>::type { \
    static_assert( !BOOST_PP_CAT(field, _initialized), "field already initialized" ); \
    instance_.BOOST_PP_CAT(field, _) = {arg}; \
    return *(BOOST_PP_CAT(after_, field) *)this; \
  } \

#define VALUABLE_WARN_WHEN_USING_CTOR(ctor) \
  template<typename A> ctor { static_assert(std::is_same<with_proxy_, A>::value, "attempted to assign to proxy object: use operator*() after with() or update()"); }; \

#define VALUABLE_WITH_PROXY_TEMPLATE_INIT_ARGS_OP(s, d, e) \
  bool BOOST_PP_CAT(e, _initialized) = false \

#define VALUABLE_WITH_PROXY_TEMPLATE_INIT_ARGS(...) \
  BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(VALUABLE_WITH_PROXY_TEMPLATE_INIT_ARGS_OP, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))) \

#define VALUABLE_WITH_PROXY_INIT_TYPEDEFS_OP2(s, d, i, e) \
  ,BOOST_PP_CAT(e, _initialized) || i == (int)T::valuable_indexes_::d \

#define VALUABLE_WITH_PROXY_INIT_TYPEDEFS_OP(s, d, e) \
    using BOOST_PP_CAT(after_, e) = \
      with_proxy_<T, check \
        BOOST_PP_SEQ_FOR_EACH_I(VALUABLE_WITH_PROXY_INIT_TYPEDEFS_OP2, e, d) \
      >; \

#define VALUABLE_WITH_PROXY_INIT_TYPEDEFS(...) \
  BOOST_PP_SEQ_FOR_EACH(VALUABLE_WITH_PROXY_INIT_TYPEDEFS_OP, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__), BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)) \

#define VALUABLE_BUILD_OPERATOR_OP(s, d, e) \
  static_assert( !check || (::valuable::is_optional<decltype(T::BOOST_PP_CAT(e, _))>::value || BOOST_PP_CAT(e, _initialized)), "field not initialized" ); \

#define VALUABLE_BUILD_OPERATOR(...) \
    inline auto operator*() { \
      BOOST_PP_SEQ_FOR_EACH(VALUABLE_BUILD_OPERATOR_OP, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)) \
      return instance_; \
    } \

#define VALUABLE_WITH_PROXY(...) \
  template<typename T, bool check = true, VALUABLE_WITH_PROXY_TEMPLATE_INIT_ARGS(__VA_ARGS__)> \
  struct with_proxy_ { \
    VALUABLE_WITH_PROXY_INIT_TYPEDEFS(__VA_ARGS__) \
    T instance_; \
    operator T() { return instance_; } \
    BOOST_PP_SEQ_FOR_EACH(VALUABLE_WITH_PROXY_FIELD, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)) \
    VALUABLE_BUILD_OPERATOR(__VA_ARGS__) \
    VALUABLE_WARN_WHEN_USING_CTOR(with_proxy_(A&&)) \
    VALUABLE_WARN_WHEN_USING_CTOR(with_proxy_(A const&)) \
    VALUABLE_WARN_WHEN_USING_CTOR(auto operator=(A const&) -> with_proxy_&) \
    VALUABLE_WARN_WHEN_USING_CTOR(auto operator=(A&&) -> with_proxy_&) \
    with_proxy_() = default; \
  }; \
  static auto with() { return with_proxy_<valuable_type_>{}; } \
  auto update() const { auto a = with_proxy_<valuable_type_, false>{}; a.instance_ = *this; return a; } \

#define VALUABLE_APPEND_UNDERSCORE(s, d, e) \
  BOOST_PP_CAT(e, _)

#define VALUABLE_APPEND_UNDERSCORES(...) \
  BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_TRANSFORM(VALUABLE_APPEND_UNDERSCORE, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)))

#define VALUABLE_FIELD_GETTER(r, d, field) \
  friend inline auto const& BOOST_PP_CAT(, field) (valuable_type_ const& v) { return v.BOOST_PP_CAT(field, _); } \

#define VALUABLE_GETTERS(...) \
  BOOST_PP_SEQ_FOR_EACH(VALUABLE_FIELD_GETTER, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define VALUABLE_FIELD_SETTER(r, d, field) \
  template<typename... Args> \
  inline auto BOOST_PP_CAT(with_, field) (Args... args) -> valuable_type_ { auto cp = *this; cp.BOOST_PP_CAT(field, _) = {args...}; return cp; } \

#define VALUABLE_SETTERS(...) \
  BOOST_PP_SEQ_FOR_EACH(VALUABLE_FIELD_SETTER, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define VALUABLE_TUPLE_FUNCTIONS(...) \
  inline auto to_tuple() const { return std::tie(VALUABLE_APPEND_UNDERSCORES(__VA_ARGS__)); } \
  inline auto to_tuple() { return std::tie(VALUABLE_APPEND_UNDERSCORES(__VA_ARGS__)); } \

#define VALUABLE_INDEXES(...) \
  enum class valuable_indexes_ { __VA_ARGS__ }; \

#define FIELDS(name, ...) \
  public: \
  VALUABLE_META(name) \
  \
  name() = default; \
  name(name const&) = default; \
  name(name&&) = default; \
  auto operator=(name const&) -> name& = default; \
  auto operator=(name&&) -> name& = default; \
  \
  VALUABLE_INDEXES(__VA_ARGS__) \
  VALUABLE_TUPLE_FUNCTIONS(__VA_ARGS__) \
  VALUABLE_COMPARISON_OPERATORS \
  VALUABLE_OSTREAM_OPERATORS(__VA_ARGS__) \
  VALUABLE_WITH_PROXY(__VA_ARGS__) \
  VALUABLE_GETTERS(__VA_ARGS__) \
  VALUABLE_SETTERS(__VA_ARGS__) \
  private:

#define TYPE(field, ...) __VA_ARGS__ BOOST_PP_CAT(field, _)

#endif
